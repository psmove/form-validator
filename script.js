const form = document.getElementById('form');
const username = document.getElementById('username');
const email = document.getElementById('email');
const password = document.getElementById('password');
const password2 = document.getElementById('password2');

//Show input error message
function showError(input, message) {
    const formControl = input.parentElement;
    formControl.className = 'form-control error';
    const small = formControl.querySelector('small');
    small.innerText = message;
}


//Show input success message
function showSuccess(input) {
    const formControl = input.parentElement;
    formControl.className = 'form-control success';
}

//Check email is valid
function checkEmail(input) {
    const re = /\S+@\S+\.\S+/;
    if(re.test(input.value.trim())) {
        showSuccess(input);
    } else{
        showError(input, 'Email недействительна');
    }
}

function checkRequired(inputArr) {
    inputArr.forEach(function(input) {
        if(input.value.trim() === '') {
            
            showError(input, `${getFieldName(input)} требуется`);
        }else{
            showSuccess(input);
        }
    });

}

//Check input length
function checkLength(input, min, max) {
    if(input.value.length < min) {
        showError(input, `${getFieldName(input)} должо быть больше ${min} символов`);
    } else if (input.value.length > max) {
        showError(input, `${getFieldName(input)} должо быть меньше ${max} символов`);
    } else {
        showSuccess(input);
    }
}

//Check passwords match
function checkPasswordsMatch(input1, input2) {
    if(input1.value !== input2.value ) {
        showError(input2, 'Пароли не совпадают');
    }
}

//Get fieldname
function getFieldName(input) {
    return input.id.charAt(0).toUpperCase() + input.id.slice(1);
}



// Event listeners
form.addEventListener('submit',function(e) {
    e.preventDefault();
    checkRequired([username,email,password,password2]);
    checkLength(username, 3, 15);
    checkLength(password, 6, 25);
    checkEmail(email);
    checkPasswordsMatch(password, password2);
});